#include <webots/robot.h>
#include <webots/supervisor.h>
#include <stdio.h>

int main() {
    wb_robot_init();

    // do this once only
    WbNodeRef robot_node = wb_supervisor_node_get_from_def("e-puck2");
    WbFieldRef trans_field = wb_supervisor_node_get_field(robot_node, "translation");

    while (wb_robot_step(32) != -1) {
        // this is done repeatedly
        const double *trans = wb_supervisor_field_get_sf_vec3f(trans_field);
        printf("MY_ROBOT is at position: %g %g %g\n", trans[0], trans[1], trans[2]);
    }

    wb_robot_cleanup();

    return 0;
}
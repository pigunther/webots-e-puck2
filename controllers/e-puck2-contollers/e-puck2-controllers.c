// #include <webots/robot.h>
// #include <webots/motor.h>
// #include <math.h>

// #define TIME_STEP 32

// int main() {
// wb_robot_init();

// WbDeviceTag motor = wb_robot_get_device("my_motor");

// double F = 2.0;   // frequency 2 Hz
// double t = 0.0;   // elapsed simulation time

// while (wb_robot_step(TIME_STEP) != -1) {
// double pos = sin(t * 2.0 * M_PI * F);
// wb_motor_set_position(motor, pos);
// t += (double)TIME_STEP / 1000.0;
// }

// wb_robot_cleanup();

// return 0;
// }

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "e-puck2-controllers.h"
#include "multiagent/movement/movement_controller.h"
#include "multiagent/bt/bt_controller.h"
#include "multiagent/debug/debug_controller.h"
#include "multiagent/model/data_models.h"

#include <webots/accelerometer.h>
#include <webots/camera.h>
#include <webots/distance_sensor.h>
#include <webots/light_sensor.h>
#include <webots/motor.h>
#include <webots/position_sensor.h>
#include <webots/robot.h>
#include <webots/emitter.h>
#include <webots/receiver.h>


int main (int argc, char *argv[]) {
    Robot robot;
    /* initialize Webots */
    wb_robot_init ();
    int time_step = 96;
    wb_robot_step (time_step);


    printf (" ----------------------------------------------------------------- \n");

    init_movement (&robot);
    init_bt_protocol (&robot);
    init_debug (&robot);

    printf("robot name: %s\n", wb_robot_get_name());

    if        (strncmp(wb_robot_get_name(), "3e-puck", 7) == 0) {
        const char  *message3 = "s800000001", *message4 = "t800000010";
        wb_emitter_send (robot.movement.emitter, message3, strlen(message3)+1);
        wb_emitter_send (robot.movement.emitter, message4, strlen(message4)+1);
    } else if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
        const char  *message3 = "s800000011", *message4 = "t800000010";
        wb_emitter_send (robot.movement.emitter, message3, strlen(message3)+1);
        wb_emitter_send (robot.movement.emitter, message4, strlen(message4)+1);
    } else if (strncmp(wb_robot_get_name(), "2e-puck", 7) == 0) {
        const char  *message3 = "s800000021", *message4 = "t800000010";
        wb_emitter_send (robot.movement.emitter, message3, strlen(message3)+1);
        wb_emitter_send (robot.movement.emitter, message4, strlen(message4)+1);
    }

    while ( wb_robot_step (time_step) != -1) {
//                printf ("%lf %lf %lf %lf %lf %lf\n", wb_distance_sensor_get_value (robot.movement.ps0),
//                                                wb_distance_sensor_get_value (robot.movement.ps1),
//                                                wb_distance_sensor_get_value (robot.movement.ps2),
//                                                wb_distance_sensor_get_value (robot.movement.ps5),
//                                                wb_distance_sensor_get_value (robot.movement.ps6),
//                                                wb_distance_sensor_get_value (robot.movement.ps7)
//                                                );

        run_movement (&robot);
        run_debug (&robot);
        run_protocol (&robot);
    }

    wb_robot_cleanup();
    return 0;
}
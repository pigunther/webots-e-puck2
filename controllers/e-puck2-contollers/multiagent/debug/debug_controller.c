//
// Created by Смирнов Влад on 2019-01-29.
//

//#include "ch.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#include "../../e-puck2-controllers.h"

#include "debug_controller.h"
#include "multiagent/model/data_models.h"
#include "multiagent/movement/movement_controller.h"
#include "multiagent/bt/bt_controller.h"
#include "../model/data_models.h"
#include <webots/emitter.h>
#include <webots/receiver.h>
//#include <uart/e_uart_char.h>
//#include <../../ChibiOS/os/hal/lib/streams/chprintf.h>




void init_debug(Robot* robot) {
    printf("init debug\n");
    Debug debug;
    debug.type = '0';
//    debug.timestamp = chVTGetSystemTime();
    debug.period = 3000;

    robot->debug = debug;
    robot->mode = '0';
}

void run_debug(Robot* robot) {

//    if (chVTGetSystemTime() > robot->debug.timestamp + robot->debug.period) {
    Message message;
    int n;
    switch (robot->debug.type) {
        case 'm':
            ;
            if (robot->movement.target_distance > robot->movement.stop_radius) {
                  // don't do anything
//                    Message message;
                n = 25;

//                    message.type = 'd';
//                    message.message = malloc(sizeof(char) * n);
//                    message.message[0] = message.type;
//                    message.message[1] = '2';
//                    message.message[2] = '2';
//                    char ans[] = "target_distance";
//                    memcpy(message.message + 3, ans, strlen(ans) * sizeof(char));
//                    double_to_string(robot->movement.target_distance, message.message, 18, 25);
//                    message.size = 25;


//                    e_send_uart1_char (message.message, message.size);
//                    while(e_uart1_sending());

//                    wb_emitter_send (robot->movement.emitter, message.message, message.size + 1);
////                    write_to_file (robot->receiver_name, (message.message));
//                    add_message(robot, &message);
//
//                    free(message.message);

            }

            break;

        case 'n':;

            message.type = 'n';
            n = 10;

            message.message = malloc(sizeof(char) * n);
            message.message[0] = message.type;
            message.message[1] = '8';
            double_to_string(robot->movement.x_real, message.message, 2, 6);
            double_to_string(robot->movement.y_real, message.message, 6, 10);
            message.size = 10;


            wb_emitter_send (robot->movement.emitter, message.message, message.size + 1);
            //add_message(robot, &message);
            free(message.message);

            break;


        case 'C':


            message.type = 'C';

            int i = 0, receiver_channel = -1, robots_num = robot->movement.robots_num;
            receiver_channel = wb_receiver_get_channel(robot->movement.receiver);

            n = 11;
            message.message = malloc(sizeof(char) * n);
            message.message[0] = message.type;
            message.message[1] = '9';
            message.message[2] = receiver_channel + '0'; //send robot num
//            double_to_string(robot->movement.y_real - robot->movement.y_target, message.message, 3, 7); //todo кажется не работает с отрицательными
            double_to_string(robot->movement.y_real, message.message, 3, 7); //todo кажется не работает с отрицательными
            double_to_string(robot->movement.vel_line_real*10, message.message, 7, 11);
            message.size = n;

            for (i = 0; i < robots_num; i++) {
                if (robot->movement.channels[i] != receiver_channel) { //не отправляем сами себе
                    wb_emitter_set_channel(robot->movement.emitter, robot->movement.channels[i]);
                    wb_emitter_send(robot->movement.emitter, message.message, message.size + 1);
                }

            }

            break;

        default:

            break;

    }

// }
}

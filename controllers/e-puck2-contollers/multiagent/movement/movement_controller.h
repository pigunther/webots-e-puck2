/*
 * movement.h
 *
 *  Created on: Jan 28, 2019
 *      Author: smirnovvlad
 */
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <math.h>
//
//#include "ch.h"
//#include "chprintf.h"
//#include "hal.h"
//#include "shell.h"
//
//#include "aseba_vm/aseba_node.h"
//#include "aseba_vm/skel_user.h"
//#include "aseba_vm/aseba_can_interface.h"
//#include "aseba_vm/aseba_bridge.h"
//#include "audio/audio_thread.h"
//#include "audio/play_melody.h"
//#include "audio/play_sound_file.h"
//#include "audio/microphone.h"
//#include "camera/po8030.h"
//#include "epuck1x/Asercom.h"
//#include "epuck1x/Asercom2.h"
//#include "epuck1x/a_d/advance_ad_scan/e_acc.h"
//#include "epuck1x/motor_led/advance_one_timer/e_led.h"
//#include "epuck1x/utility/utility.h"
//#include "sensors/battery_level.h"
//#include "sensors/imu.h"
//#include "sensors/mpu9250.h"
//#include "sensors/proximity.h"

//#include "button.h"
//#include "cmd.h"
//#include "config_flash_storage.h"
//#include "exti.h"
//#include "fat.h"
//#include "i2c_bus.h"
//#include "ir_remote.h"
//#include "leds.h"
//#include <main.h>
//#include "memory_protection.h"
//#include "motors.h"
//#include "sdio.h"
//#include "selector.h"
//#include "spi_comm.h"
//#include "usbcfg.h"
//#include "communication.h"
//#include "uc_usage.h"

#ifndef MOVEMENT_MOVEMENT_H_
#define MOVEMENT_MOVEMENT_H_

#include "movement_controller.h"
#include "multiagent/model/data_models.h"

void init_movement(Robot* robot);

void run_movement(Robot* robot);

void set_goal_coordinates(Robot* robot, double x_new_target, double y_new_target);
void set_goal_velocities(Robot* robot, double x_new_target);
void set_own_coordinates(Robot* robot, double x_new_target, double y_new_target);

void calculate_coordiantes_to_robot (Robot* robot, double x_new_target, double y_new_target);


#endif /* SRC_MOVEMENT_MOVEMENT_H_ */

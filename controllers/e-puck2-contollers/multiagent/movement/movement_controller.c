#include "movement_controller.h"
#include "math.h"
#include <string.h>




#include "../../e-puck2-controllers.h"
#include "../model/data_models.h"

#include <stdio.h>
#include <stdlib.h>
#include <webots/robot.h>
#include <webots/motor.h>
#include <webots/position_sensor.h>
#include <webots/distance_sensor.h>
#include <webots/differential_wheels.h>
#include <webots/emitter.h>
#include <webots/receiver.h>




//// Описание робота:
#define H  0.0265   //Полурасстояние между колесами [м].
#define AX_H  0.035   //Полурасстояние между колесами [м].
#define MAX_VEL_LINE_WHEEL 0.14 //Часть скорости колеса, используемая для линейной скорости центра [м/с].
#define MAX_VEL_ANGLE_WHEEL (double)(1.2 * 0.041 * M_PI - MAX_VEL_LINE_WHEEL)   //Часть скорости колеса, используемая для угловой скорости центра [м/с]. (1.2*0.041*M_PI - максимальная линейная скорость колеса).
#define MAX_VEL_LINE (double)(0.5 * MAX_VEL_LINE_WHEEL) //Максимальная лин. скорость центра [м/с].
#define MAX_VEL_ANGLE (double)(MAX_VEL_ANGLE_WHEEL / H) //Макс. угл. скорость робота [рад/с].
#define ACC_LINE (double)(0.1 * MAX_VEL_LINE)   //Линейное ускорение [м/с].
#define ACC_ANGLE (double)(0.25 * MAX_VEL_ANGLE)    //Угловое ускорение [рад/с].
#define TO_WEBOTS_VEL_FORMAT 0.00628
#define TIME_STEP 96


void Update_goal_point_distance(Robot *robot);

void Update_goal_point_angle(Robot *robot);

void Update_angle_disagreement(Robot *robot);

void Update_odometer2(Robot *robot);

void Update_goal_speeds_values(Robot *robot);

void update_target_speeds_values(Robot *robot);

void Calculate_speeds(Robot *robot);

void Set_wheels_speeds(Robot *robot);

void Set_wheels_speeds(Robot *robot);

void init_movement(Robot *robot);

void run_movement(Robot *robot);

void move(Robot* robot);

void set_goal_coordinates(Robot *robot, double x_new_target, double y_new_target);
void set_goal_velocities(Robot *robot, double x_new_target);

void set_own_coordinates(Robot *robot, double x_new_target, double y_new_target);

void Update_moving_vector_for_potential_method(Robot* robot);

void Update_obstacle_repulsion_vector(Robot *robot);

double Get_proximity_value_mm(int data_from_sensor);

void Filter_and_update_proximity_values(Robot *robot);

void find_angle(Robot *robot);

double get_angle_between(double x1, double y1, double x2, double y2);


void init_movement(Robot* robot) {

    printf("init_movement\n");

    Movement movement;

    //// Описание робота:
    movement.x_real = 0.0;
    movement.y_real = 0.0; //Реальные коорд-ты центра робота [м].
    movement.theta = M_PI / 2; //Угол ориентации относительно Ox [рад].
    movement.vel_line_real = 0.0; //Реальная линейная скорость [м/с].
    movement.vel_angle_real = 0.0; //Реальная угловая скорость [рад/с].
    movement.vel_left_ms = 0.0;
    movement.vel_right_ms = 0.0; //Скорость лев. и прав. колес в [м/с].
    movement.vel_left_steps = 0;
    movement.vel_right_steps = 0.0; //Скорость лев. и прав. колес в [шаги/с].

    //// Описание целевой точки:
    movement.x_target = 0.0;
    movement.y_target = 0.0;
    movement.dec_radius = 0.07; //В этом радиусе до цели - торможение [м].
    movement.stop_radius = 0.05; //В этом радиусе от цели - стоп [м].
    movement.dec_angle = (45.0 / 180.0) * M_PI; //В этом угле начинается торможение [рад].
    movement.target_angle = 0.0; //Угол на цель отн. оси Ox [рад]. Goal_point_angle
    movement.target_distance = 0.0; //Расстояние до цели [м].

    //// Целевые параметры робота:
    movement.vel_line_target = 0.0; //Целевая линейная скорость [м/с].
    movement.vel_angle_target = 0.0; //Целевая угловая скорость [рад/с].
    movement.target_step_angle = 0.0; //Это условный целевой угол отн. оси Ox, который будет все время меняться [рад]. Goal_angle
    movement.disagreement_angle = 0.0; //Рассогласование по углу [рад].

    //// Переменные для работы колесной одометрии:
    movement.left_encoder_old = 0; //Количество шагов на левом колесе пердыдущее [шаги].
    movement.right_encoder_old = 0; //Количество шагов на правом колесе пердыдущее [шаги].

    //// Данные для реализации метода потенциальных сил:
    movement.potential_method_switch = 0;

    movement.sens_val_num = 5;

    //// Данные для получения вектора отталкивания от препятствий:
//    int prox_0_init[movement.sens_val_num];
//    int prox_1_init[movement.sens_val_num];
//    int prox_2_init[movement.sens_val_num];
//    int prox_5_init[movement.sens_val_num];
//    int prox_6_init[movement.sens_val_num];
//    int prox_7_init[movement.sens_val_num];
//
//    movement.prox_0_sens_values = prox_0_init; //Значения, подаваемые в медианный фильтр для каждого сенсора.
//    movement.prox_1_sens_values = prox_1_init;
//    movement.prox_2_sens_values = prox_2_init;
//    movement.prox_5_sens_values = prox_5_init;
//    movement.prox_6_sens_values = prox_6_init;
//    movement.prox_7_sens_values = prox_7_init;
//
//    int filtered_init[6];
//    double proximity_val_init[6];
//    double sensor_angles_init[6] = {1.309, 0.785, 0.0, M_PI, 2.356, 1.885};
//
//    movement.filtered_proximity_value = filtered_init; //Отфильтрованные значения.
//    movement.proximity_values_mm = proximity_val_init; //Значения дальностей [мм].
    movement.sensor_angles[0] = 1.309;  //Углы расположения ИК-датчиков на роботе (отн. поперечной оси) [рад]. //todo check?
    movement.sensor_angles[1] = 0.785;  //Углы расположения ИК-датчиков на роботе (отн. поперечной оси) [рад].
    movement.sensor_angles[2] = 0.0;    //Углы расположения ИК-датчиков на роботе (отн. поперечной оси) [рад].
    movement.sensor_angles[3] = M_PI;   //Углы расположения ИК-датчиков на роботе (отн. поперечной оси) [рад].
    movement.sensor_angles[4] = 2.356;  //Углы расположения ИК-датчиков на роботе (отн. поперечной оси) [рад].
    movement.sensor_angles[5] = 1.885;  //Углы расположения ИК-датчиков на роботе (отн. поперечной оси) [рад].

    movement.obstacle_repulsion_vector_length = 0;
    movement.obstacle_repulsion_vector_angle = 0.0;

    //// Данные для получения вектора отталкивания от других агентов:
    movement.n_robots = 1; //Общее количество агентов (включая текущего).
    movement.agents_repulsion_vector_length = 0;
    movement.agents_repulsion_vector_angle = 0;

    //// Результирующий вектор движения:
    movement.vel_potential_method = 0.0;
    movement.angle_potential_method = 0.0;
    movement.init_theta = movement.theta;

    movement.width = 0.05;

    movement.left_motor = wb_robot_get_device("left wheel motor");
    movement.right_motor = wb_robot_get_device("right wheel motor");
    movement.left_motor_sensor = wb_robot_get_device("right wheel sensor");
    movement.right_motor_sensor = wb_robot_get_device("right wheel sensor");

    movement.left_encoder_actual = 0;
    movement.right_encoder_actual = 0;

    movement.channels[0] = 1;
    movement.channels[1] = 2;
    movement.channels[2] = 3;
    movement.robots_num = 3; //todo repear n_robots
    movement.n_neighbors = 2;
    movement.a = -1;
    movement.f = 0.5;
    movement.g = 0.5;

    double a_indexes[9] = {4., -2., -2., -1., 2., -1., -4., -3., 7.}; //now it is laplas matrix
    int i;
    for (i=0; i < (movement.robots_num * movement.robots_num); i++) {
        movement.a_indexes[i] = a_indexes[i];
        movement.f_indexes[i] = -((double)(i+1))/movement.robots_num/2; //now useless
        movement.g_indexes[i] = -((double)(i+1))/movement.robots_num; //now useless
    }


    robot->movement = movement;

    wb_motor_set_position(robot->movement.left_motor, INFINITY);
    wb_motor_set_position(robot->movement.right_motor, INFINITY);
    wb_motor_set_velocity(robot->movement.left_motor, 0.0);
    wb_motor_set_velocity(robot->movement.right_motor, 0.0);

    WbDeviceTag emitter = wb_robot_get_device ("emitter");
    WbDeviceTag receiver = wb_robot_get_device ("receiver");
    WbDeviceTag ps0 = wb_robot_get_device ("ps0");
    WbDeviceTag ps1 = wb_robot_get_device ("ps1");
    WbDeviceTag ps2 = wb_robot_get_device ("ps2");
    WbDeviceTag ps3 = wb_robot_get_device ("ps3");
    WbDeviceTag ps4 = wb_robot_get_device ("ps4");
    WbDeviceTag ps5 = wb_robot_get_device ("ps5");
    WbDeviceTag ps6 = wb_robot_get_device ("ps6");
    WbDeviceTag ps7 = wb_robot_get_device ("ps7");
//    const int emitter_channel = wb_emitter_get_channel (emitter);
//    const int receiver_channel = wb_emitter_get_channel (receiver);
    wb_receiver_enable (receiver, TIME_STEP);
    wb_distance_sensor_enable(ps0, TIME_STEP);
    wb_distance_sensor_enable(ps1, TIME_STEP);
    wb_distance_sensor_enable(ps2, TIME_STEP);
    wb_distance_sensor_enable(ps3, TIME_STEP);
    wb_distance_sensor_enable(ps4, TIME_STEP);
    wb_distance_sensor_enable(ps5, TIME_STEP);
    wb_distance_sensor_enable(ps6, TIME_STEP);
    wb_distance_sensor_enable(ps7, TIME_STEP);

    robot->movement.ps0 = ps0;
    robot->movement.ps1 = ps1;
    robot->movement.ps2 = ps2;
    robot->movement.ps3 = ps3;
    robot->movement.ps4 = ps4;
    robot->movement.ps5 = ps5;
    robot->movement.ps6 = ps6;
    robot->movement.ps7 = ps7;

    robot->movement.emitter = emitter;
    robot->movement.receiver = receiver;



//    wb_differential_wheels_enable_encoders(1000);
//    printf("%f left encoder \n", wb_differential_wheels_get_left_encoder());
    //todo delete this shit code
    robot->movement.device = wb_robot_get_device(wb_robot_get_model());
    robot->name = wb_robot_get_name();
    if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
        robot->receiver_name = "2e-puck";
    } else {
        robot->receiver_name = "e-puck";
    }

    robot->tact_num = 0;

}

// simplified function
void run_movement(Robot* robot) {
    switch (robot->mode) {
        case 'm': // Движение к точке

            move(robot);

            break;

        case 'C': //

            Update_odometer2(robot);

            //пока линейная модель
            robot->movement.vel_angle_real = 0;
            robot->movement.vel_angle_target = 0;
            double y_current_delta = robot->movement.y_real;
            //todo поменять x на y
            int index = wb_receiver_get_channel(robot->movement.receiver) - 1;

//            double acc = robot->movement.a_indexes[index] * robot->movement.vel_line_real
//                    + robot->movement.f_indexes[index] * (robot->movement.y_delta + y_current_delta * robot->movement.n_neighbors)
//                    + robot->movement.g_indexes[index] * (robot->movement.vel_delta + robot->movement.vel_line_real * robot->movement.n_neighbors);
            double acc = -robot->movement.y_delta - robot->movement.a_indexes[index * robot->movement.robots_num + index] * robot->movement.y_real;
           if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
                printf("acc %lf, nn_delta %lf, y_delta %lf, ind_i %d, v_real %lf\n",
                        acc,
                        robot->movement.y_delta,
                        y_current_delta,
                        index * robot->movement.robots_num + index,
                        robot->movement.vel_line_real);
            }
            set_goal_velocities(robot, acc);
            //наверное надо добавить функцию для подсчета всей херни
            Calculate_speeds (robot);
            Set_wheels_speeds(robot); //устанавливаем наши координаты

            robot->debug.type = 'C';
            break;
        default:
            break;
    }
}

void move(Robot* robot) {
    int c = 4; //todo get_selector() create it
    switch(c) //todo get_selector() create it
    {
        /// простое движение в точку
        case 0:

            Update_odometer2(robot); //Обновляет положение центра робота и его угол ориентации.
            Update_goal_point_distance(robot);


            break;

            /// Робот отворачивается от препядствия
        case 1:

            Filter_and_update_proximity_values(robot);
            for (int i = 0; i < 6; i++) {
                robot->movement.proximity_values_mm[i] = Get_proximity_value_mm(robot->movement.filtered_proximity_value[i]);
            }
            Update_obstacle_repulsion_vector(robot);
            robot->movement.target_step_angle = robot->movement.obstacle_repulsion_vector_angle;
//            robot->movement.target_distance = 0;

            Update_odometer2(robot);
            Update_angle_disagreement(robot);
            Update_goal_speeds_values(robot);
            Calculate_speeds(robot);
            Set_wheels_speeds(robot);
//            chThdSleepMilliseconds(95);
//
//            if (SDU1.config->usbp->state == USB_ACTIVE)
//            {
//                chprintf((BaseSequentialStream *)&SDU1, "Theta = %lf, Obstacle repulse angle = %lf\r\n", Theta*180/M_PI, Obstacle_repulsion_vector_angle*180/M_PI);
//            }
            break;

        case 2:
            robot->movement.potential_method_switch = 1;

//            T_nach = chVTGetSystemTime();

            //Обновляем положение и угол ориентации (одометрия):
            Update_odometer2(robot);

            //Обновляем расстояние до целевой точки и угол на целевую точку:
            Update_goal_point_distance(robot);
            Update_goal_point_angle(robot);

            //Обновляем угол и модуль вектора отталкивания от препятствий:
            Filter_and_update_proximity_values(robot); //Обновление дальностей с дальнмеров.
            for (int i = 0; i < 6; i++) {
                robot->movement.proximity_values_mm[i] = Get_proximity_value_mm(robot->movement.filtered_proximity_value[i]);
            }
            Update_obstacle_repulsion_vector(robot); //Обновление угла и модуля вектора отталкивания.

            //Обновляем угол и значение целевой скорости движения:
            Update_moving_vector_for_potential_method(robot);

            robot->movement.target_step_angle = robot->movement.angle_potential_method;
            //Теперь у нас есть целевая скорость и целевой угол - вычисляем рассогласование по углу:
            Update_angle_disagreement(robot);

//				if(Goal_point_distance <= Stop_radius)
//				{
//					Update_angle_disagreement(&Angle_disagreement, Theta, M_PI/2);
//				}

            //Определение целевой угловой и линейной скорости:
            Update_goal_speeds_values(robot);

            //Вычисляем реальные скорости для колес и устанавливаем их на колеса:
            Calculate_speeds(robot);
            Set_wheels_speeds(robot);

//            if (SDU1.config->usbp->state == USB_ACTIVE)
//            {
//                chprintf((BaseSequentialStream *)&SDU1, "Theta = %lf, Obst_angle = %lf, Obst_mod = %lf, Goal_sp = %lf, goal_angl = %lf, Angle_disagr = %lf\r\n", Theta*180/M_PI, Obstacle_repulsion_vector_angle*180/M_PI, Obstacle_repulsion_vector_length, Moving_speed_value_potential_method, Moving_angle_potential_method*180/M_PI, Angle_disagreement);
//            }

//            while(chVTGetSystemTime() <  (T_nach + 100));

            break;
        case 3:

//            if (true) {
//            if (chVTGetSystemTime() > robot->debug.timestamp + robot->debug.period) {

//            T_nach = chVTGetSystemTime();

                //Обновляем положение и угол ориентации (одометрия):
                Update_odometer2(robot);

                //Обновляем расстояние до целевой точки и угол на целевую точку:
                Update_goal_point_distance(robot);
                Update_goal_point_angle(robot);

                //Обновляем угол и модуль вектора отталкивания от препятствий:
                Filter_and_update_proximity_values(robot); //Обновление дальностей с дальнмеров.
                for (int i = 0; i < 6; i++) {
                    robot->movement.proximity_values_mm[i] = Get_proximity_value_mm(
                            robot->movement.filtered_proximity_value[i]);
                }

                if (robot->tact_num % 3 == 0) {
                    find_angle(robot); //Обновление угла и модуля вектора отталкивания.

                    //Обновляем угол и значение целевой скорости движения:
//            Update_moving_vector_for_potential_method(robot);

                    //Теперь у нас есть целевая скорость и целевой угол - вычисляем рассогласование по углу:
                    Update_angle_disagreement(robot);

                    //Определение целевой угловой и линейной скорости:
                    update_target_speeds_values(robot);
                }
                robot->tact_num++;

                //Вычисляем реальные скорости для колес и устанавливаем их на колеса:
                Calculate_speeds(robot);
                Set_wheels_speeds(robot);

//            if (SDU1.config->usbp->state == USB_ACTIVE)
//            {
//                chprintf((BaseSequentialStream *)&SDU1, "Theta = %lf, Obst_angle = %lf, Obst_mod = %lf, Goal_sp = %lf, goal_angl = %lf, Angle_disagr = %lf\r\n", Theta*180/M_PI, Obstacle_repulsion_vector_angle*180/M_PI, Obstacle_repulsion_vector_length, Moving_speed_value_potential_method, Moving_angle_potential_method*180/M_PI, Angle_disagreement);
//            }

//            while(chVTGetSystemTime() <  (T_nach + 100));
//            }
            break;

        default:
            break;

    }
}

//void calculate_rotation_angle(Robot* robot) {
//    robot->movement.rotation_angle = robot->movement.theta + atan(robot->movement.vel_x_target / robot->movement.vel_y_target);
//}
//
/**
 * Устанавливает целевые координаты точки.
 **/
void set_goal_coordinates(Robot* robot, double x_new_target, double y_new_target) {

    robot->movement.x_target = x_new_target;
    robot->movement.y_target = y_new_target;
}

/**
 * Устанавливает целевые координаты точки.
 **/
void set_goal_velocities(Robot* robot, double v_x_new_target) {

//    robot->movement.vel_x_target = v_x_new_target;
//    robot->movement.vel_y_target = v_x_new_target;
    robot->movement.vel_line_target = v_x_new_target; //одинаковые скорости на колеса
}


/**
 * Устанавливает собственные координаты робота.
 **/
void set_own_coordinates(Robot* robot, double x, double y) {

    robot->movement.x_real = x;
    robot->movement.y_real = y;
}

void calculate_coordiantes_to_robot (Robot* robot, double x_new_target, double y_new_target) {
    robot->movement.x_real = x_new_target - robot->movement.width;
    robot->movement.y_real = y_new_target - robot->movement.width;
}


void Update_goal_point_distance(Robot* robot) { //double *Goal_point_distance, double X_pos_actual, double Y_pos_actual, double X_goal_point, double Y_goal_point)  {
    //Обновляет значение расстояния до цели.
    if(robot->movement.x_real == robot->movement.x_target && robot->movement.y_real == robot->movement.y_target) //Если координаты робота и цели совпадают.
        robot->movement.target_distance = 0; //[м]
    else //Иначе вычисление расстояния (гипотенузы по двум катетам).
        robot->movement.target_distance = sqrt(pow(robot->movement.x_target - robot->movement.x_real, 2) + pow(robot->movement.y_target - robot->movement.y_real, 2)); //[м]
}

/**
 * Функция обновляет угол ориентации до цели для точки с роботом относительно Ox, [рад] на основе текущего положения робота.
 * Т.е. это тот угол, на который робот должен повернуть, чтобы быть направленным к цели.
 * Здесь не продуман случай, когда робот достиг целевой точки и угол уже никак нельзя
 * вычислить
 **/
void Update_goal_point_angle(Robot* robot) { //double *Goal_point_angle, double X_pos_actual, double Y_pos_actual, double X_goal_point, double Y_goal_point)  {

    int Quadrant; //Квадрант определяется для системы координат, центр которой совпадает с центром робота, а оси параллельны глобальной СК.
    if ((robot->movement.x_target - robot->movement.x_real) * (robot->movement.y_target - robot->movement.y_real) == 0)
        Quadrant = 0; //Неопределенный квадрант (точка находится на оси).
    if ((robot->movement.x_target - robot->movement.x_real) * (robot->movement.y_target - robot->movement.y_real) >
        0) //Определение квадранта точки на плоскости.
        Quadrant = (((robot->movement.x_target - robot->movement.x_real) > 0 &&
                     (robot->movement.y_target - robot->movement.y_real) > 0) ? 1 : 3);
    if ((robot->movement.x_target - robot->movement.x_real) * (robot->movement.y_target - robot->movement.y_real) < 0)
        Quadrant = (((robot->movement.x_target - robot->movement.x_real) > 0 &&
                     (robot->movement.y_target - robot->movement.y_real) < 0) ? 4 : 2);

    if ((robot->movement.x_target - robot->movement.x_real) != 0 ||
        (robot->movement.y_target - robot->movement.y_real) != 0)
        robot->movement.target_angle = asin(fabs(robot->movement.y_target - robot->movement.y_real) /
                                            sqrt(pow(robot->movement.x_target - robot->movement.x_real, 2)
                                                 + pow(robot->movement.y_target - robot->movement.y_real,
                                                       2))); //Вычисление угла прямоугольного треугольника с катетами X_goal и Y_goal, [рад].

    else Quadrant = 0;

    switch (Quadrant) //Вычисление угла в локальной СК в зависимости от квадранта.
    {
        case 1:
            break;
        case 2:
            robot->movement.target_angle = M_PI - robot->movement.target_angle;
            break;
        case 3:
            robot->movement.target_angle = M_PI + robot->movement.target_angle;
            break;
        case 4:
            robot->movement.target_angle = 2 * M_PI - robot->movement.target_angle;
            break;
        case 0:
            if ((robot->movement.x_target - robot->movement.x_real) == 0 &&
                (robot->movement.y_target - robot->movement.y_real) > 0)
                robot->movement.target_angle = M_PI / 2; //90 градусов.
            if ((robot->movement.x_target - robot->movement.x_real) == 0 &&
                (robot->movement.y_target - robot->movement.y_real) < 0)
                robot->movement.target_angle = 1.5 * M_PI; //270 градусов.
            if ((robot->movement.x_target - robot->movement.x_real) > 0 &&
                (robot->movement.y_target - robot->movement.y_real) == 0)
                robot->movement.target_angle = 0; //0 градусов.
            if ((robot->movement.x_target - robot->movement.x_real) < 0 &&
                (robot->movement.y_target - robot->movement.y_real) == 0)
                robot->movement.target_angle = M_PI; //180 градусов.
            if ((robot->movement.x_target - robot->movement.x_real) == 0 &&
                (robot->movement.y_target - robot->movement.y_real) == 0)
                robot->movement.target_angle = M_PI / 2; //Позиция робота совпадает с целевой точкой.
            break;
    }
}

/**
 * Обновление угла рассогласования с учетом рациональности поворота (чтобы робот поворачивал на угол, меньший или равный 180 град.
 * На входе два угла в [рад] - угол ориентации робота отн. оси Ох (Theta) и целевой угол (Goal_angle), так же отн. оси Ох.
 * На выходе - угол в [рад], обозначающий рассогласование.
 * При этом модуль |величина рассогласования| всегда <= 180 град.
 **/
void Update_angle_disagreement(Robot* robot) { //double *Angle_disagreement, double Theta, double Goal_angle)  {

    robot->movement.disagreement_angle = (robot->movement.target_step_angle - robot->movement.theta);

    if((robot->movement.target_step_angle - robot->movement.theta) > M_PI) {
        robot->movement.disagreement_angle = -(2 * M_PI - (robot->movement.target_step_angle - robot->movement.theta));
    }
    if((robot->movement.target_step_angle - robot->movement.theta) < -M_PI) {
        robot->movement.disagreement_angle = 2 * M_PI + (robot->movement.target_step_angle - robot->movement.theta);
    }
}

/**
 * Функция обновляет положение робота и угол его ориентации
 * на основе старых значений энкодеров и текущих координат и угла
 * ориентации.
 **/
void Update_odometer2(Robot* robot) { //double *Left_encoder_old, double *Right_encoder_old, double *Theta, double *X_pos_actual, double *Y_pos_actual) {
    //Колесная одометрия.

    double left_encoder_actual = 0; //Количество шагов на левом колесе [шаги].
    double right_encoder_actual = 0; //Количество шагов на правом колесе [шаги].
    double left_length = 0; //Расстояние, пройденное левым колесом [м].
    double right_length = 0; //Расстояние, пройденное правым колесом [м].
    double alpha_odometer = 0; //Угол сегмента окружности при повороте [рад].
    double radius_odometer = 0; //Радиус сегмента окружности при повороте [м].
    double center_x_odometer = 0, center_y_odometer = 0; //Координаты положения центра окружности поворота [м][м].

    left_encoder_actual = robot->movement.left_encoder_actual; //Обновляем данные энкодеров.
    right_encoder_actual = robot->movement.right_encoder_actual; //Получаем актуальные значения [шаги].

    right_length = ((right_encoder_actual - robot->movement.right_encoder_old) / 1000) * 0.1288; //Количество пройденных шагов делим на количество шагов в одном обороте колеса, получаем количество оборотов колеса, умножаем на длинну окружности колеса.
    left_length = ((left_encoder_actual - robot->movement.left_encoder_old) / 1000) * 0.1288; //[м].

    if (right_length != left_length) {
        alpha_odometer = (right_length - left_length) / 0.053; //В радианах. 0.053 - расстояние между колесами [м].
        radius_odometer = left_length / alpha_odometer;

        center_x_odometer = robot->movement.x_real - (radius_odometer + 0.0265) * sin(robot->movement.theta); //0.0265 - полурасстояние между колесами [м].
        center_y_odometer = robot->movement.y_real - (radius_odometer + 0.0265) * (-cos(robot->movement.theta));

        robot->movement.theta = fmod((robot->movement.theta + alpha_odometer + 2 * M_PI), 2 * M_PI);

        robot->movement.x_real = center_x_odometer + (radius_odometer + 0.0265) * sin(robot->movement.theta);
        robot->movement.y_real = center_y_odometer + (radius_odometer + 0.0265) * (-cos(robot->movement.theta));
    }

    if (right_length == left_length) {
        robot->movement.x_real = robot->movement.x_real  + left_length * cos(robot->movement.theta);
        robot->movement.y_real = robot->movement.y_real + left_length * sin(robot->movement.theta);
    }

    robot->movement.left_encoder_old = left_encoder_actual; //Обновляем данные энкодеров.
    robot->movement.right_encoder_old = right_encoder_actual; //Получаем актуальные значения [шаги].
}

/**
 * Обновляются целевые угловая и линейная скорость исходя из расстояния до цели и угла рассогласования
 * (выдаваемая скорость не превышает максимального значения)
 **/
void Update_goal_speeds_values(Robot* robot) { //double *Linear_speed_goal, double *Angular_speed_goal, double Goal_point_distance, double Dec_radius, double Stop_radius, double Linear_speed_max, double Angle_disagreement, double Angular_speed_max, double Dec_angle, uint8_t Potential_method_switch, double Moving_speed_value_potential_method)  {

    //Вычисление целевой линейной скорости:
    switch (robot->movement.potential_method_switch) {
        case 0:
            if (robot->movement.target_distance > robot->movement.dec_radius) {
                robot->movement.vel_line_target = MAX_VEL_LINE; //До радиуса торможения модуль линейной скорости максимален.
            } else {
                if (robot->movement.target_distance <= robot->movement.dec_radius && robot->movement.target_distance > robot->movement.stop_radius) {
                    robot->movement.vel_line_target = (robot->movement.target_distance / robot->movement.dec_radius) * MAX_VEL_LINE; //Внутри радиуса торможения модуль линейной скорости пропорционален расстоянию до цели.
                } else {
                    if (robot->movement.target_distance <= robot->movement.stop_radius) {
                        robot->movement.vel_line_target = 0; //Внутри радиуса остановки - остановка.
                    }
                }
            }
            break;

        case 1: //Включен метод потенц. полей.
            robot->movement.vel_line_target = robot->movement.vel_potential_method;

            if (robot->movement.target_distance <= robot->movement.stop_radius) {
                robot->movement.vel_line_target = 0; //Внутри радиуса остановки - остановка.
            }
            break;
    }

    //Вычисление целевой угловой скорости:
    if (fabs(robot->movement.disagreement_angle) <= 0.015) { //Если рассогласование лежит в малых пределах, то угловая целевая скорость - ноль.
        robot->movement.vel_angle_target = 0;
    } else {
        if (fabs(robot->movement.disagreement_angle) <= robot->movement.dec_angle) { //Если угол рассогласования меньше или равен углу торможения, то торможение.
            robot->movement.vel_angle_target = (robot->movement.disagreement_angle / robot->movement.dec_angle) * MAX_VEL_ANGLE;
        } else {
            robot->movement.vel_angle_target = (fabs(robot->movement.disagreement_angle) / (robot->movement.disagreement_angle)) * MAX_VEL_ANGLE;
        }
    }
}

/**
 * Обновляет реальную угловую и линейную скорости колес с учетом ускорения и текущей угловой и линейной скорости.
 * Под "реальной" понимается та скорость, которая будет установлена на колеса.
 * Это сделано для того, чтобы был эффект инерционности и скорость менялась не скачком, а с нарастанием (ограниченное приращение скорости).
 **/
void Calculate_speeds(Robot* robot) { //double *Linear_speed_real, double *Angular_speed_real, double Linear_acc, double Angular_acc, double Linear_speed_goal, double Angular_speed_goal)  {

    //Вычисление реальной линейной скорости:
    /*Если разница между целевой и реальной скоростью превышает величину возможного ускорения,
    	то можно изменить скорость только на величину ускорения*/
//    if (fabs(robot->movement.vel_line_target - robot->movement.vel_line_real) > ACC_LINE) {
//        robot->movement.vel_line_real = robot->movement.vel_line_real + (fabs(robot->movement.vel_line_target - robot->movement.vel_line_real) \
//                / (robot->movement.vel_line_target - robot->movement.vel_line_real)) * ACC_LINE;
//
//    } else {
        //Если разница между целевой и реальной скоростью не превышает величину возможного ускорения, то сразу переходим к целевой скорости.
    robot->movement.vel_line_real = robot->movement.vel_line_target;
//    }

    //Вычисление реальной угловой скорости (логика такая же, что и с линейной):
    if (fabs(robot->movement.vel_angle_target - robot->movement.vel_angle_real) > ACC_ANGLE) {
        robot->movement.vel_angle_real = robot->movement.vel_angle_real + (fabs(robot->movement.vel_angle_target - robot->movement.vel_angle_real) \
                / (robot->movement.vel_angle_target - robot->movement.vel_angle_real)) * ACC_ANGLE;
    } else {
        robot->movement.vel_angle_real = robot->movement.vel_angle_target;
    }

    if (fabs(robot->movement.vel_line_real) > MAX_VEL_LINE_WHEEL ) {
        if (robot->movement.vel_line_real > 0) {
            robot->movement.vel_line_real = MAX_VEL_LINE_WHEEL;
        } else {
            robot->movement.vel_line_real = -MAX_VEL_LINE_WHEEL;
        }
    }
}

/**
 * Эта функция устанавливает скорости на колеса, переводя требуемую линейную и угловую скорости в [шаги/с] для каждого колеса.
 **/
void Set_wheels_speeds(Robot* robot) { //double Linear_speed_real, double Angular_speed_real, double h)  {

    //Положительная угл. скорость - против часовой стрелки.
    robot->movement.vel_left_ms = robot->movement.vel_line_real - H * robot->movement.vel_angle_real;
    robot->movement.vel_right_ms = robot->movement.vel_line_real + H * robot->movement.vel_angle_real;

    //Из метров в секунду в шаги в секунду:
//    robot->movement.vel_left_steps =  (int16_t) 1200 * robot->movement.vel_left_ms / (1.2 * M_PI * 0.041) * TO_WEBOTS_VEL_FORMAT;
    robot->movement.vel_left_steps =  robot->movement.vel_left_ms / (41.0 / 1000.0 / 2.0);
//    robot->movement.vel_right_steps = (int16_t) 1200 * robot->movement.vel_right_ms / (1.2 * M_PI * 0.041) * TO_WEBOTS_VEL_FORMAT;
    robot->movement.vel_right_steps = robot->movement.vel_left_ms / (41.0 / 1000.0 / 2.0);

    //Установка:
    wb_motor_set_velocity(robot->movement.left_motor, robot->movement.vel_left_steps);
    wb_motor_set_velocity(robot->movement.right_motor, robot->movement.vel_right_steps);

    robot->movement.left_encoder_actual = robot->movement.left_encoder_actual + robot->movement.vel_left_steps * 1200 / 2 / M_PI * TIME_STEP/1000; //old + speed * ms
//    robot->movement.left_encoder_actual = robot->movement.left_encoder_actual + robot->movement.vel_left_steps / TO_WEBOTS_VEL_FORMAT * TIME_STEP / 1000; //old + speed * ms
    robot->movement.right_encoder_actual = robot->movement.right_encoder_actual + robot->movement.vel_right_steps * 1200 / 2 / M_PI  * TIME_STEP/1000; //old + speed * ms
//    robot->movement.right_encoder_actual = robot->movement.right_encoder_actual + robot->movement.vel_right_steps / TO_WEBOTS_VEL_FORMAT * TIME_STEP / 1000; //old + speed * ms
//    right_motor_set_speed(robot->movement.vel_right_steps);
   if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
        printf("--------speed rad in sec:------------\n");
        printf("rad: %lf\n", robot->movement.vel_left_steps);
        printf("grad: %lf\n", robot->movement.vel_left_steps * 180.0 / M_PI);
        printf("--------end speed---------\n");
    }
}

/**
 * На вход подается 6 массивов размерности 5 с данными с каждого переднего ИК-дальномера.
 * Применяется медианный фильтр (подходит под задачу, так как в последовательности данных с ИК-дальномера
 * есть одиночные мощные импульсы, а остальные данные достаточно схожи). На выходе фильтра - массив размерности 6 с отфильтрованными
 * значениями для каждого ИК-дальномера.
 **/
//void Filter_and_update_proximity_values(Robot *robot) {
//    robot->movement.filtered_proximity_value[0] = wb_distance_sensor_get_value(robot->movement.ps0);
//    robot->movement.filtered_proximity_value[1] = wb_distance_sensor_get_value(robot->movement.ps1);
//    robot->movement.filtered_proximity_value[2] = wb_distance_sensor_get_value(robot->movement.ps2);
//    robot->movement.filtered_proximity_value[3] = wb_distance_sensor_get_value(robot->movement.ps5);
//    robot->movement.filtered_proximity_value[4] = wb_distance_sensor_get_value(robot->movement.ps6);
//    robot->movement.filtered_proximity_value[5] = wb_distance_sensor_get_value(robot->movement.ps7);
//}
void Filter_and_update_proximity_values(Robot *robot) {
    //int *Prox_0_sens_values, int *Prox_1_sens_values, int *Prox_2_sens_values, int *Prox_5_sens_values, int *Prox_6_sens_values, int *Prox_7_sens_values, int Size, int *Filtered_proximity_value)  {

    int temp;

    /// Занесение неотфильтрованных данных в массивы:
    for (int i = 0; i < robot->movement.sens_val_num; i++) {
        robot->movement.prox_0_sens_values[i] = wb_distance_sensor_get_value(robot->movement.ps0);
        robot->movement.prox_1_sens_values[i] = wb_distance_sensor_get_value(robot->movement.ps1);
        robot->movement.prox_2_sens_values[i] = wb_distance_sensor_get_value(robot->movement.ps2);
        robot->movement.prox_5_sens_values[i] = wb_distance_sensor_get_value(robot->movement.ps5);
        robot->movement.prox_6_sens_values[i] = wb_distance_sensor_get_value(robot->movement.ps6);
        robot->movement.prox_7_sens_values[i] = wb_distance_sensor_get_value(robot->movement.ps7);
        wb_robot_step(16);
    }

    /// Сортировка по возрастанию для каждого массива:
    for (int i = 0; i < robot->movement.sens_val_num - 1; i++) {
        for (int j = 0; j < robot->movement.sens_val_num - i - 1; j++) {
            if (robot->movement.prox_0_sens_values[j] > robot->movement.prox_0_sens_values[j + 1]) {
                temp = robot->movement.prox_0_sens_values[j];
                robot->movement.prox_0_sens_values[j] = robot->movement.prox_0_sens_values[j + 1];
                robot->movement.prox_0_sens_values[j + 1] = temp;
            }
            if (robot->movement.prox_1_sens_values[j] > robot->movement.prox_1_sens_values[j + 1]) {
                temp = robot->movement.prox_1_sens_values[j];
                robot->movement.prox_1_sens_values[j] = robot->movement.prox_1_sens_values[j + 1];
                robot->movement.prox_1_sens_values[j + 1] = temp;
            }
            if (robot->movement.prox_2_sens_values[j] > robot->movement.prox_2_sens_values[j + 1]) {
                temp = robot->movement.prox_2_sens_values[j];
                robot->movement.prox_2_sens_values[j] = robot->movement.prox_2_sens_values[j + 1];
                robot->movement.prox_2_sens_values[j + 1] = temp;
            }
            if (robot->movement.prox_5_sens_values[j] > robot->movement.prox_5_sens_values[j + 1]) {
                temp = robot->movement.prox_5_sens_values[j];
                robot->movement.prox_5_sens_values[j] = robot->movement.prox_5_sens_values[j + 1];
                robot->movement.prox_5_sens_values[j + 1] = temp;
            }
            if (robot->movement.prox_6_sens_values[j] > robot->movement.prox_6_sens_values[j + 1]) {
                temp = robot->movement.prox_6_sens_values[j];
                robot->movement.prox_6_sens_values[j] = robot->movement.prox_6_sens_values[j + 1];
                robot->movement.prox_6_sens_values[j + 1] = temp;
            }
            if (robot->movement.prox_7_sens_values[j] > robot->movement.prox_7_sens_values[j + 1]) {
                temp = robot->movement.prox_7_sens_values[j];
                robot->movement.prox_7_sens_values[j] = robot->movement.prox_7_sens_values[j + 1];
                robot->movement.prox_7_sens_values[j + 1] = temp;
            }
        }
    }

    robot->movement.filtered_proximity_value[0] = robot->movement.prox_0_sens_values[2];
    robot->movement.filtered_proximity_value[1] = robot->movement.prox_1_sens_values[2];
    robot->movement.filtered_proximity_value[2] = robot->movement.prox_2_sens_values[2];
    robot->movement.filtered_proximity_value[3] = robot->movement.prox_5_sens_values[2];
    robot->movement.filtered_proximity_value[4] = robot->movement.prox_6_sens_values[2];
    robot->movement.filtered_proximity_value[5] = robot->movement.prox_7_sens_values[2]; //todo ask why [2]? because of median filter?
}


/**
 * Возвращает значение расстояния до препятствия в [мм].
 * На основе значения данных с какого-либо ИК-дальномера.
 * Входное значение - данные с сенсора. Используется аппроксимация четырьмя графиками.
 * На вход подается отфильтрованное значение с одного ИК-датчика.
 **/
double Get_proximity_value_mm(int data_from_sensor) {

    double exp = 2.7182818284590;

    double coef[5] = {-162.62943851, 218.75339461, -70.79223948, 9.06062872, -0.41364544};

    if (data_from_sensor > 2800) {
        return 1;
    }
    if (data_from_sensor <= 0) {
        return 70;
    }
    double log_data = log(data_from_sensor);
    double ans = 0;
    int i = 0;
    for(i=0; i<5; i++) {
        ans += coef[i] * pow(log_data, i);
    }
    if (ans > 60) return 60;
    return ans;



    // return 0;

    //todo ask don't understand the numbers here?
}

/**
 * На вход подается массив с отфильтрованными расстояниями в [мм] до препятствий от 6-ти передних ИК_дальномеров.
 * Затем вычисляется направление (угол [рад]) и модуль результирующего вектора отталкивания относительно глобальной оси Ox.
 **/
void Update_obstacle_repulsion_vector(Robot *robot) {
    //double Proximity_values_mm[], double Theta, double Sensor_angles[], double *Vector_angle, double *Vector_length)  {


    double x_repulse = 0.0; //Суммарный X всех векторов.
    double y_repulse = 0.0; //Суммарный Y всех векторов.

   for (int i = 0; i < 6; i++) //Вычисление координат результирующего вектора.
       {
           // Если больше 60 [мм], считаем, что препятствия нет (предел видимости ИК-дальномера).
   //        if (robot->movement.proximity_values_mm[i] >= 60.0)
   //        {
   //            continue;
   //        }
           switch (i) {
               case 0:
               case 5:
                   if (robot->movement.proximity_values_mm[i] >= 50.0)
                   {
                       continue;
                   }
                   x_repulse = x_repulse + (60.0 - robot->movement.proximity_values_mm[i]) / 60.0 * cos(robot->movement.sensor_angles[i]);
                   y_repulse = y_repulse + (60.0 - robot->movement.proximity_values_mm[i]) / 60.0 * sin(robot->movement.sensor_angles[i]);

                   break;

               case 1:
               case 4:
                   if (robot->movement.proximity_values_mm[i] >= 40.0)
                   {
                       continue;
                   }
                   x_repulse = x_repulse + (40.0 - robot->movement.proximity_values_mm[i]) / 40.0 * cos(robot->movement.sensor_angles[i]);
                   y_repulse = y_repulse + (40.0 - robot->movement.proximity_values_mm[i]) / 40.0 * sin(robot->movement.sensor_angles[i]);


                   break;

               case 2:
               case 3:
                   if (robot->movement.proximity_values_mm[i] >= 30.0)
                   {
                       continue;
                   }
                   x_repulse = x_repulse + (30.0 - robot->movement.proximity_values_mm[i]) / 30.0 * cos(robot->movement.sensor_angles[i]);
                   y_repulse = y_repulse + (30.0 - robot->movement.proximity_values_mm[i]) / 30.0 * sin(robot->movement.sensor_angles[i]);

                   break;

           }
   //        x_repulse = x_repulse + (60.0 - robot->movement.proximity_values_mm[i]) / 60.0 * cos(robot->movement.sensor_angles[i]);
   //        y_repulse = y_repulse + (60.0 - robot->movement.proximity_values_mm[i]) / 60.0 * sin(robot->movement.sensor_angles[i]);
       }


    if (x_repulse == 0.0 && y_repulse == 0.0) //Если препятствий нет, то модуль вектора отталкивания равен 0.
    {
        robot->movement.obstacle_repulsion_vector_length = 0;
        robot->movement.obstacle_repulsion_vector_angle = robot->movement.theta;
    } else //Если препятствия есть, то нормируем модуль до единицы.
    {
        //Угол отн. поперечной подвижнойоси, закрепленной на роботе (а нужен относительно глобальной оси Ox) [рад].
        robot->movement.obstacle_repulsion_vector_angle = acos(x_repulse / sqrt(pow(x_repulse, 2) + pow(y_repulse, 2)));
        robot->movement.obstacle_repulsion_vector_angle = fmod(robot->movement.obstacle_repulsion_vector_angle +
                (robot->movement.theta - M_PI / 2) + 2 * M_PI + M_PI, 2 * M_PI); //Угол отн. Ox [рад].
        robot->movement.obstacle_repulsion_vector_length = sqrt(pow(x_repulse, 2) + pow(y_repulse, 2));
    }
}

/**
 * На входе - угол и модуль вектора отталкивания от препятствий и угол до целевой точки.
 * На выходе - направление и скорость те, которые вычислены для метода потенциалов.
 **/
void Update_moving_vector_for_potential_method(Robot* robot) {
    // double Obstacle_repulsion_vector_angle, double Obstacle_repulsion_vector_length, double Goal_point_angle, double *Moving_speed_value_potential_method, double *Moving_angle_potential_method, double Linear_speed_max)  {

    double x = 0.0;
    double y = 0.0;
    double vector_length = 0.0;

    if(robot->movement.obstacle_repulsion_vector_length != 0.0) //Вектор отталкивания от препятствий не нулевой.
    {
        x = cos(robot->movement.obstacle_repulsion_vector_angle) + cos(robot->movement.target_angle); //Считаем координаты результирующего вектора.
        y = sin(robot->movement.obstacle_repulsion_vector_angle) + sin(robot->movement.target_angle);
        vector_length = sqrt(pow(x, 2)+ pow(y, 2)); //Вычисляем модуль.

        robot->movement.vel_potential_method = (vector_length / 2.0) * MAX_VEL_LINE; //Значение скорости.
        robot->movement.angle_potential_method = fmod(acos(x / vector_length) + 2 * M_PI, 2 * M_PI); //Угол направления.
    }
    else //Вектор отталкивания от препятствий нулевой.
    {
        robot->movement.vel_potential_method = MAX_VEL_LINE; //Значение скорости.
        robot->movement.angle_potential_method = robot->movement.target_angle; //Угол направления.
    }
}

/**
 * Обновляются целевые угловая и линейная скорость исходя из расстояния до цели и угла рассогласования
 * (выдаваемая скорость не превышает максимального значения)
 **/
void update_target_speeds_values(Robot *robot) {

    //Вычисление целевой линейной скорости:

    if (robot->leader_id != 0 && robot->leader_id != -1 && robot->movement.target_distance > 0.2) {
        robot->movement.vel_line_target = MAX_VEL_LINE * 1.5; //До радиуса торможения модуль линейной скорости максимален.
    } else if (robot->movement.target_distance > robot->movement.dec_radius) {
        robot->movement.vel_line_target = MAX_VEL_LINE; //До радиуса торможения модуль линейной скорости максимален.
    } else {
        if (robot->movement.target_distance <= robot->movement.dec_radius &&
            robot->movement.target_distance > robot->movement.stop_radius) {
            robot->movement.vel_line_target = (robot->movement.target_distance / robot->movement.dec_radius) *
                                              MAX_VEL_LINE; //Внутри радиуса торможения модуль линейной скорости пропорционален расстоянию до цели.
        } else {
            if (robot->movement.target_distance <= robot->movement.stop_radius) {
                robot->movement.vel_line_target = 0; //Внутри радиуса остановки - остановка.
            }
        }
    }

    //Вычисление целевой угловой скорости:
    if (fabs(robot->movement.disagreement_angle) <=
        0.015) { //Если рассогласование лежит в малых пределах, то угловая целевая скорость - ноль.
        robot->movement.vel_angle_target = 0;
    } else {
        if (fabs(robot->movement.disagreement_angle) <=
            robot->movement.dec_angle) { //Если угол рассогласования меньше или равен углу торможения, то торможение.
            robot->movement.vel_angle_target =
                    (robot->movement.disagreement_angle / robot->movement.dec_angle) * MAX_VEL_ANGLE;
        } else {
            if (fabs(robot->movement.disagreement_angle) < M_PI / 2) {
                robot->movement.vel_angle_target =
                        fabs(robot->movement.disagreement_angle) / (robot->movement.disagreement_angle) * MAX_VEL_ANGLE / 2;
            } else {
                robot->movement.vel_angle_target =
                        (fabs(robot->movement.disagreement_angle) / (robot->movement.disagreement_angle)) *
                        MAX_VEL_ANGLE;
            }
        }
    }
}

/**
 * На вход подается массив с отфильтрованными расстояниями в [мм] до препятствий от 6-ти передних ИК_дальномеров.
 * Затем вычисляется направление (угол [рад]) и модуль результирующего вектора отталкивания относительно глобальной оси Ox.
 **/
void find_angle(Robot *robot) {
    //double Proximity_values_mm[], double Theta, double Sensor_angles[], double *Vector_angle, double *Vector_length)  {

    double relative_angle = 0.0;

    double h0 = 0.0;
    double h1 = 0.0;
    double h2 = 0.0;
    double h3 = 0.0;
    double h4 = 0.0;
    double h5 = 0.0;

//    double a,b,c,e,f, phi;

    h0 = AX_H * 1000 + robot->movement.proximity_values_mm[0];
    h1 = AX_H * 1000 + robot->movement.proximity_values_mm[1];
    h2 = AX_H * 1000 + robot->movement.proximity_values_mm[2];
    h3 = AX_H * 1000 + robot->movement.proximity_values_mm[3];
    h4 = AX_H * 1000 + robot->movement.proximity_values_mm[4];
    h5 = AX_H * 1000 + robot->movement.proximity_values_mm[5];

    bool edge05 = false;
    bool edge01 = false;
    bool edge54 = false;
    bool edge12 = false;
    bool edge43 = false;

    bool top0 = false;
    bool top1 = false;
    bool top2 = false;
    bool top3 = false;
    bool top4 = false;
    bool top5 = false;

    /// Поймем, с какой стороны находится цель относительно оси робота

    double d = (cos(robot->movement.theta) * (robot->movement.y_target - robot->movement.y_real)) -
            ((robot->movement.x_target - robot->movement.x_real) * sin(robot->movement.theta));

    // если d = 0 - цель на прямой, > 0 - слева, < 0 - справа

    if (robot->movement.proximity_values_mm[0] <= 50.0) top0 = true;

    if (robot->movement.proximity_values_mm[5] <= 50.0) top5 = true;

    if (robot->movement.proximity_values_mm[1] <= 50.0) top1 = true;

    if (robot->movement.proximity_values_mm[4] <= 50.0) top4 = true;

    if (robot->movement.proximity_values_mm[2] <= 45.0) top2 = true;

    if (robot->movement.proximity_values_mm[3] <= 45.0) top3 = true;

    if (top3 && top4) edge43 = true;

    if (top4 && top5) edge54 = true;

    if (top5 && top0) edge05 = true;

    if (top0 && top1) edge01 = true;

    if (top1 && top2) edge12 = true;

    double cur_tar_angle = get_angle_between(cos(robot->movement.theta), sin(robot->movement.theta),
            robot->movement.x_target - robot->movement.x_real, robot->movement.y_target - robot->movement.y_real);

    cur_tar_angle = d < 0 ? M_PI / 2 - cur_tar_angle : cur_tar_angle + M_PI / 2;

    if (cur_tar_angle > M_PI * 2) {
        cur_tar_angle -= M_PI * 2;
    }

    if (cur_tar_angle > M_PI * 2) {
        cur_tar_angle -= M_PI * 2;
    }

    if (cur_tar_angle < -M_PI * 2) {
        cur_tar_angle += M_PI * 2;
    }

    if (cur_tar_angle < 0) {
        cur_tar_angle += M_PI * 2;
    }

    if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
       printf("d_angle,d=%lf,cur=%lf", d, cur_tar_angle);

        printf("\n,h0=%lf,h1=%lf,h2=%lf"
                 ",h3=%lf,h4=%lf,h5=%lf\n", h0, h1, h2, h3, h4, h5);
    }


    if (cur_tar_angle >= robot->movement.sensor_angles[2] && cur_tar_angle <= robot->movement.sensor_angles[1]) { /// цель в ребре 21
        if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
                       printf("goal point in 21, top2:%d, top1:%d, cur_tar_angle:%lf\n", top2, top1, cur_tar_angle);
                       printf("theta: %lf, x: %lf, y:%lf\n", robot->movement.theta, robot->movement.x_real, robot->movement.y_real);

        }
//        if (!top2 && !top1) { /// путь свободен
//            relative_angle = cur_tar_angle;
//        } else if (top2 && top1) {
//            relative_angle = M_PI/2
//                             - atan( (h1 * cos(robot->movement.sensor_angles[1]) - h2) / ( h1 * sin(robot->movement.sensor_angles[1])) )
//                             + M_PI * (30.0 - robot->movement.proximity_values_mm[2]) / (30.0 * 24.0);
//        } else if (top2) {
//            relative_angle = robot->movement.sensor_angles[1] +
//                             (robot->movement.sensor_angles[0] - robot->movement.sensor_angles[1]) * (30.0 - robot->movement.proximity_values_mm[2]) / 30.0;
//        } else if (top1) {
//            relative_angle = robot->movement.sensor_angles[2];
//        }

        if (!top2 && !top1) { /// путь свободен
            relative_angle = cur_tar_angle;
        } else if (top2 && top1) {
            relative_angle = atan( (h2 * sin(robot->movement.sensor_angles[2]) - h1 * sin(robot->movement.sensor_angles[1])) /
                                    ( h2 * cos(robot->movement.sensor_angles[2] - h1 * cos(robot->movement.sensor_angles[1])) ));
//                             - M_PI * (40.0 - robot->movement.proximity_values_mm[4]) / (40.0 * 24.0);
        } else if (top2) {

            relative_angle = robot->movement.sensor_angles[2] + M_PI/2;
        } else if (top1) {
            relative_angle = robot->movement.sensor_angles[1] + M_PI/2;
        }

    } else if (cur_tar_angle >= robot->movement.sensor_angles[4] && cur_tar_angle <= robot->movement.sensor_angles[3]) { /// цель в ребре 43
        if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
                       printf("goal point in 43, top4:%d, top3:%d, cur_tar_angle:%lf\n", top4, top3, cur_tar_angle);

                       printf("theta: %lf, x: %lf, y:%lf\n", robot->movement.theta, robot->movement.x_real, robot->movement.y_real);
        }
        if (!top4 && !top3) { /// путь свободен
            relative_angle = cur_tar_angle;
        } else if (top4 && top3) {
            relative_angle = atan( (h4 * sin(robot->movement.sensor_angles[4]) - h3 * sin(robot->movement.sensor_angles[3])) /
                                     ( h4 * cos(robot->movement.sensor_angles[4] - h3 * cos(robot->movement.sensor_angles[3])) ))
                             - M_PI * (30.0 - robot->movement.proximity_values_mm[3]) / (30.0 * 24.0);
        } else if (top3) {
//            relative_angle = robot->movement.sensor_angles[4] +
//                             (robot->movement.sensor_angles[5] - robot->movement.sensor_angles[4]) * (30.0 - robot->movement.proximity_values_mm[3]) / 30.0;
            relative_angle = robot->movement.sensor_angles[3] - M_PI/2;
        } else if (top4) {
            relative_angle = robot->movement.sensor_angles[4] - M_PI/2;
        }

    } else if (cur_tar_angle >= robot->movement.sensor_angles[1] && cur_tar_angle <= robot->movement.sensor_angles[0]) { /// цель в ребре 10
        if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
               printf("goal point in 10, top1:%d, top0:%d, cur_tar_angle:%lf\n", top1, top0, cur_tar_angle);

                       printf("theta: %lf, x: %lf, y:%lf\n", robot->movement.theta, robot->movement.x_real, robot->movement.y_real);
        }
//        if (!top1 && !top0) { /// путь свободен
//            relative_angle = cur_tar_angle;
//        } else if (top0 && top1) {
//            relative_angle = robot->movement.sensor_angles[1]
//                             + M_PI/2
//                             - atan( (h0 * cos(robot->movement.sensor_angles[0] - robot->movement.sensor_angles[1]) - h1) /
//                             ( h0 * sin(robot->movement.sensor_angles[0] - robot->movement.sensor_angles[1])) )
//                             + M_PI * (40.0 - robot->movement.proximity_values_mm[1]) / (40.0 * 24.0);
//        } else if (top0) {
//            relative_angle = robot->movement.sensor_angles[0] +
//                             (robot->movement.sensor_angles[5] - robot->movement.sensor_angles[0]) * (40.0 - robot->movement.proximity_values_mm[1]) / 40.0;
//        } else if (top1) {
//            relative_angle = robot->movement.sensor_angles[1];
//        }

        if (!top0 && !top1) { /// путь свободен
            if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
                       printf("%lf %lf %lf %lf %lf %lf \n",
                       robot->movement.proximity_values_mm[0],
                       robot->movement.proximity_values_mm[1],
                       robot->movement.proximity_values_mm[2],
                       robot->movement.proximity_values_mm[3],
                       robot->movement.proximity_values_mm[4],
                       robot->movement.proximity_values_mm[5] );

                       printf("theta: %lf, x: %lf, y:%lf\n", robot->movement.theta, robot->movement.x_real, robot->movement.y_real);
                }
            if (top2) {
                relative_angle = robot->movement.sensor_angles[2] + M_PI/2;
            } else {
                relative_angle = cur_tar_angle;
            }
        } else if (top0 && top1) {
            relative_angle = atan( (h1 * sin(robot->movement.sensor_angles[1]) - h0 * sin(robot->movement.sensor_angles[0])) /
                                    ( h1 * cos(robot->movement.sensor_angles[1] - h0 * cos(robot->movement.sensor_angles[0])) ));
//                             - M_PI * (40.0 - robot->movement.proximity_values_mm[4]) / (40.0 * 24.0);
        } else if (top0) {
            relative_angle = robot->movement.sensor_angles[0] + M_PI/2;
        } else if (top1) {
            relative_angle = robot->movement.sensor_angles[1] + M_PI/2;
        }

    } else if (cur_tar_angle >= robot->movement.sensor_angles[5] && cur_tar_angle <= robot->movement.sensor_angles[4]) { /// цель в ребре 54
        if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
           printf("goal point in 54, top5:%d, top4:%d, cur_tar_angle:%lf\n", top5, top4, cur_tar_angle);

                       printf("theta: %lf, x: %lf, y:%lf\n", robot->movement.theta, robot->movement.x_real, robot->movement.y_real);
        }
        if (!top4 && !top5) { /// путь свободен
            relative_angle = cur_tar_angle;
        } else if (top4 && top5) {
            relative_angle = atan( (h5 * sin(robot->movement.sensor_angles[5]) - h4 * sin(robot->movement.sensor_angles[4])) /
                                    ( h5 * cos(robot->movement.sensor_angles[5] - h4 * cos(robot->movement.sensor_angles[4])) ));
//                             - M_PI * (40.0 - robot->movement.proximity_values_mm[4]) / (40.0 * 24.0);
        } else if (top5) {
//            relative_angle = robot->movement.sensor_angles[4] +
//                             (robot->movement.sensor_angles[0] - robot->movement.sensor_angles[5]) * (40.0 - robot->movement.proximity_values_mm[4]) / 40.0;
                relative_angle = robot->movement.sensor_angles[5] - M_PI/2;
        } else if (top4) {
            relative_angle = robot->movement.sensor_angles[4] - M_PI/2;
        }

    } else if (cur_tar_angle >= robot->movement.sensor_angles[0] && cur_tar_angle <= robot->movement.sensor_angles[5]) { /// цель в ребре 05
        if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
           printf("goal point in 05, top0:%d, top5:%d, cur_tar_angle:%lf\n", top0, top5, cur_tar_angle);

                       printf("theta: %lf, x: %lf, y:%lf\n", robot->movement.theta, robot->movement.x_real, robot->movement.y_real);
        }
        if (robot->movement.proximity_values_mm[0] >= 40.0 && robot->movement.proximity_values_mm[5] >= 40.0) { /// путь свободен
            relative_angle = cur_tar_angle;

            if (edge43) {

                relative_angle = M_PI/2
                                 + atan( (h4 * cos(robot->movement.sensor_angles[3] - robot->movement.sensor_angles[4]) - h3) /
                                         ( h4 * sin(robot->movement.sensor_angles[3] - robot->movement.sensor_angles[4])) );
//                                 + M_PI * (30.0 - robot->movement.proximity_values_mm[3]) / (30.0 * 24.0);
            }

            if (edge12) {
                relative_angle = M_PI/2
                                 - atan( (h1 * cos(robot->movement.sensor_angles[1]) - h2) / ( h1 * sin(robot->movement.sensor_angles[1])) );
//                                 - M_PI * (30.0 - robot->movement.proximity_values_mm[2]) / (30.0 * 24.0);
            }

        } else if (top5 || top0) {
            relative_angle = M_PI / 2;
            if (h5 < h0) {
                relative_angle = atan( (h0 * sin(robot->movement.sensor_angles[0]) - h5 * sin(robot->movement.sensor_angles[5])) /
                                                                     ( h0 * cos(robot->movement.sensor_angles[0] - h5 * cos(robot->movement.sensor_angles[5])) ));
            } else if (h5 > h0) {
                relative_angle = M_PI - atan( (h5 * sin(robot->movement.sensor_angles[5]) - h0 * sin(robot->movement.sensor_angles[0])) /
                                                                                 ( h5 * cos(robot->movement.sensor_angles[5] - h0 * cos(robot->movement.sensor_angles[0])) ));
            }
//            relative_angle = h5 > h0 ?
//                    get_angle_between(1,0,
//                    h5 * cos(cur_tar_angle) - h0 * cos(cur_tar_angle),
//                    h5 * sin(cur_tar_angle) - h0 * sin(cur_tar_angle))
//                    :
//                    - get_angle_between(
//                    h0 * cos(cur_tar_angle) - h5 * cos(cur_tar_angle),
//                    h0 * sin(cur_tar_angle) - h5 * sin(cur_tar_angle),
//                    1,0);

        }


    } else { /// цель - сзади
        relative_angle = cur_tar_angle;
    }

    robot->movement.target_step_angle = relative_angle;

        if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
            printf("rel_angle=%lf\n", relative_angle);
        }



}
double get_angle_between(double x1, double y1, double x2, double y2) {
    return acos((x1 * x2 + y1 * y2) / (sqrt(x1 * x1 + y1 * y1) * (sqrt(x2 * x2 + y2 * y2) )));
}

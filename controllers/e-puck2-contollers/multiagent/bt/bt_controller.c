#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#include "multiagent/model/data_models.h"
#include "multiagent/movement/movement_controller.h"
#include "bt_controller.h"
#include "../../e-puck2-controllers.h"
#include "../model/data_models.h"

#include <webots/receiver.h>



//
//#define IR_RECEIVER
//
//#define uart1_send_static_text(msg) do { e_send_uart1_char(msg,sizeof(msg)-1); while(e_uart1_sending()); } while(0)
//#define uart1_send_text(msg) do { e_send_uart1_char(msg,strlen(msg)); while(e_uart1_sending()); } while(0)
//#define uart2_send_static_text(msg) do { e_send_uart2_char(msg,sizeof(msg)-1); while(e_uart2_sending()); } while(0)
//#define uart2_send_text(msg) do { e_send_uart2_char(msg,strlen(msg)); while(e_uart2_sending()); } while(0)

#define MAX_MES_NUM 10
#define MAX_AGENTS  10
#define MES_TIMEOUT 1000000

//extern char buffer[BUFFER_SIZE];
extern int selector; //extern int selector;
extern char c;

int read_mes(char* buffer, Message* message);
void init_bt_protocol(Robot*robot);
void run_protocol(Robot* robot);
void send_message(Robot* robot);
int add_message(Robot* robot, Message* message);
int process_message(Robot* robot, Message* message);
int string_to_double(char* message, double* v, int from, int to);
int double_to_string(double num, char* message, int from, int to);



void init_bt_protocol(Robot* robot) {

//    e_acc_calibr ();
    printf("init_bt_protocol\n");

    Agent* agents = (Agent*)malloc(sizeof(Agent) * MAX_AGENTS);

    robot->agents = agents;
    robot->agents_num = 0;
    robot->leader_id = -1;

    robot->message_num = 0;
    robot->messages = malloc(sizeof(Message) * MAX_MES_NUM);
}

void run_protocol(Robot* robot) {
//    Message message;
//    message.type = 'm';
//    message.size = 8;
//    message.message = "0800000020";
//
//    process_message(robot, &message);
    char c = '0';
    int queue_length = wb_receiver_get_queue_length (robot->movement.receiver);

    robot->movement.y_delta = 0;
    robot->movement.vel_delta = 0;
    if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
    //            printf("readed");
    //            printf("buffer read: %s\n", buffer);
        printf("queue length: %d\n", queue_length);
    }

    while (queue_length > 0) {
        const char *buffer = wb_receiver_get_data (robot->movement.receiver);
        c = buffer[0];
        if (c == 'f' || c == 0 || c == '0') {

            return;
        } else if ((int8_t) c < 0) { // bytes

        } else if (c > 0) { // ascii code

            Message message;
            message.type = c;

            if (read_mes(buffer, &message)) {
                process_message(robot, &message);
            }
        }
        wb_receiver_next_packet(robot->movement.receiver);
        queue_length = wb_receiver_get_queue_length (robot->movement.receiver);
    }
//    send_message(robot); todo хз что надо send-ить

}

int read_mes(char* buffer, Message* message) {

    if (strlen(buffer) < 2) {
        return -1;
    }
    message->size = buffer[1] - '0';

    if (message->size == 0) {
        return -1;
    }

    message->message = buffer;

    return 1;
}

void send_message(Robot* robot) {

//    uart1_send_static_text("d09read_mes1");
    if (robot->message_num > 0) {

//        uart1_send_static_text("d09read_mes2");
        int i;
        Message* messages = robot->messages;
        for (i = 0; i < robot->message_num; i++) {

//            uart1_send_static_text("d09read_mes3");
            switch (messages[i].type) {

                case 'd':
                    switch (robot->mode) {
                        case 'm':
//                            e_send_uart1_char (messages[i].message, messages[i].size);
//                            while(e_uart1_sending());
//                            free(messages[i].message);

                        break;
                    }
                    break;

                default:

                    break;
            }
        }

        free(robot->messages);
        robot->messages = malloc(sizeof(Message) * MAX_MES_NUM);
        robot->message_num = 0;
    }
}

int process_message(Robot* robot, Message* message) {

    int num;
    double x = 0;
    double y = 0;
    int var_size = message->size / 2;

    switch (message->type) {

        case 'm':

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }
            set_goal_coordinates(robot, x/100, y/100);


            if (robot->leader_id == 0) {
                robot->debug.type = 'n';
            } else {
                robot->debug.type = 'm';
            }


            robot->mode = 'm';
            message->type = '0';

            return 1;

        case 's':

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }

            set_own_coordinates(robot, x/100, y/100);
            set_goal_coordinates(robot, x/100, y/100);

            robot->mode = 'm';

            if (robot->leader_id == 0) {
                robot->debug.type = 'n';
            } else {
                robot->debug.type = 'm';
            }

            message->type = '0';

            return 1;

        case 'n':

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }

            set_goal_coordinates (robot, x/100, y/100);

            robot->debug.type = 'm';
            robot->mode = 'm';

            message->type = '0';

            return 1;

        case 'l':

            if (message->size != 4) {
                return -1;
            }
            num = (message->message[2] - '0') * 10 + message->message[3] - '0';

//            if (SDU1.config->usbp->state == 4) {
//                chprintf ((BaseSequentialStream *) &SDU1,
//                          "num=%d\r\n", num);
//            }

            Agent agent;

            if (num >= 0 && num < 100) {
                agent.s_num = num;
            } else {
                return -1;
            }

//            if (SDU1.config->usbp->state == 4) {
//                chprintf ((BaseSequentialStream *) &SDU1,
//                          "mes[4]=%c\r\n", message->message[4]);
//            }

            if (message->message[4] == 'L') { /// L - Leader
                agent.isLeader = 1;
            } else if (message->message[4] == 'F') { /// F - Follower
                agent.isLeader = 0;
            } else {
                return -1;
            }

//            if (SDU1.config->usbp->state == 4) {
//                chprintf ((BaseSequentialStream *) &SDU1,
//                          "mes[5]=%c\r\n", message->message[5]);
//            }

            if (message->message[5] == 'S') { /// S - Set

                robot->agents[0] = agent;
                if (agent.isLeader) {
                    robot->leader_id = 0;
                }
            } else if (message->message[5] == 'I') { /// I - Info

                robot->agents[++robot->agents_num] = agent;
                if (agent.isLeader) {
                    robot->leader_id = robot->agents_num;
                }
            } else {
                return -1;
            }

            return 1;
        case 't':
            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }
            set_goal_coordinates(robot, x/100, y/100);


            robot->mode = '0';
            message->type = '0';
            robot->debug.type = 'C';

            return 1;
        case 'C':
            var_size = (message->size - 1) / 2;
            int robot_num_from = message->message[2] - '0' - 1;
            int robot_num = wb_receiver_get_channel(robot->movement.receiver) - 1;
            double v_x = 0;
            if (!string_to_double(message->message, &x, 3, 3 + var_size)) {
                free(message->message);
                return -1;
            }

             if (!string_to_double(message->message, &v_x, 3 + var_size, 3 + var_size * 2)) {
                 free(message->message);
                 return -1;
             }
             v_x = v_x/1000;

//             set_goal_coordinates(robot, x/100, 0);
//             set_goal_velocities(robot, v_x/1000, 0);

            robot->movement.y_delta += robot->movement.a_indexes[robot_num * robot->movement.robots_num + robot_num_from] *  x/100;//todo init
//            robot->movement.vel_delta -= v_x;//todo init
           if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
                printf("nn_delta %lf, x %lf, it %d, from %d\n",
                        robot->movement.y_delta,
                        x/100,
                        robot_num ,
                        robot_num_from);
            }

            robot->mode = 'C';
            message->type = '0';
            robot->debug.type = 'C';

           if (strncmp(wb_robot_get_name(), "1e-puck", 7) == 0) {
                printf("read x from message %lf\n", x);
            }

            return 1;

    }

    robot->debug.type = '0';
    robot->mode = '0';
    free(message->message);
    return -1;

}

int add_message(Robot* robot, Message* message) {

    if (robot->message_num < MAX_MES_NUM) {
        robot->messages[robot->message_num++] = *message;

//        uart1_send_static_text("d07add_mes");

        return 1;
    }

    return 0;
}

/**
 * Parse string to double, sensitive to sign and dot
 * @param message - string
 * @param v - result variable
 * @param from - start of number in message
 * @param to - end of number in message
 * @return 1 - everything ok, -1 - parsing error
 */
int string_to_double(char* message, double* v, int from, int to) {

    double x = *v;

    char mes_el;
    short dot_flag = 0;
    int negative = 1;
    for (int i = from; i < to; i++) {
        mes_el = message[i];

        if (mes_el >= '0' && mes_el <= '9') {
            if (dot_flag) {
                x = x + ((double)(mes_el - '0')) / pow(10, dot_flag);
                dot_flag++;
            } else {
                x = x * 10 + (mes_el - '0');
            }
        }

        if (mes_el == '.') {
            if (dot_flag == 0) {
                dot_flag++;
            } else {
                return -1;
            }
        }

        if (mes_el == '-') {
            if (i == from) {
                negative = -1;
            } else {
                return -1;
            }
        }

        if (mes_el == '+') {
            if (i == from) {
                negative = 1;
            } else {
                return -1;
            }
        }
    }
    x = x * negative;

    *v = x;

    return 1;

}

/**
 * Convert double to string, it takes meters and converts to centimeters
 * @param num double to be parsed
 * @param message where put string
 * @param from
 * @param to
 * @return 1 - everything ok, -1 - parsing error
 */
int double_to_string(double num, char* message, int from, int to) {

    int int_num = round(num * 100);
    int is_negative = 0;

    if (int_num < 0) {

        is_negative = 1;
        int_num = -int_num;
    }

    if (pow(10, to - from - 1) <= int_num) {

        return -1;
    }

    int n;
    int i = from;

    if (is_negative) {

        message[i++] = '-';
    }

    for (; i < to; i++) {

        n = (int)(int_num / pow(10, to - (i + 1))) % 10;
        message[i] = '0' + n;
    }


    return 1;
}
//
// Created by Смирнов Влад on 2019-01-28.
//

#ifndef E_PUCK2_MAIN_PROCESSOR_BT_CONNECTION_H
#define E_PUCK2_MAIN_PROCESSOR_BT_CONNECTION_H

#include "multiagent/model/data_models.h"


void init_bt_protocol(Robot* robot);

void run_protocol(Robot* robot);

int add_message(Robot* robot, Message* message);

int string_to_double(char* message, double* v, int from, int to);

int double_to_string(double num, char* message, int from, int to);


#endif //E_PUCK2_MAIN_PROCESSOR_BT_CONNECTION_H
